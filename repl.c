#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static char buffer[2048];

/* Define our own readline function */
char* readline(char* prompt) {
  fputs(prompt, stdout);
  fgets(buffer, 2048, stdin);
  char* cpy = malloc(strlen(buffer)+1);
  strcpy(cpy, buffer);
  cpy[strlen(cpy)-1] = '\0';
  return cpy;
}

/* Define our own add_history */
void add_history(char* unused) {}

/* Operating system != Windows: */


int main(int argc, char** argv) {
  puts("Lisp Version 0.0.0.1");
  puts("Press Ctr+C to exit.\n");

  while(1) {
    char* input = readline("lispy> ");
    add_history(input);

    printf("No you're a %s\n", input);
    free(input);
  }

  return 0;
}
