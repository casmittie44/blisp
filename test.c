#include <stdio.h>

#ifdef _WIN32
void p() {
  puts("Windows Alert!");
}

#elif defined (__CYGWIN__)
void p() {
  puts("Cygwin Alert!");
}

#endif

int main() {
  p();
  return 0;
  
}
